﻿using System;
using Otus.Prototype.Models;

namespace Otus.Prototype
{
    class Program
    {
        static void Main(string[] args)
        {
            CloneCatDemo();
            CloneMyCatDemo();
            CloneHumanDemo();
            CloneProgrammerDemo();
            CloneStudentDemo();
        }

        static void CloneCatDemo()
        {
            var originalCat = new Cat(1, Sex.Female, "Plusha");
            var myCloneCat = originalCat.MyClone();
            var cloneCat = (Cat)originalCat.Clone();
            
            originalCat.GrowUp();
            originalCat.Speak();
            
            Console.WriteLine(originalCat.ToString());
            Console.WriteLine(myCloneCat.ToString());
            Console.WriteLine(cloneCat.ToString());
        }
        
        static void CloneMyCatDemo()
        {
            var originalMyCat = new MyCat(1, Sex.Female, "Plusha") {RabbitTails = 3};
            var myCloneMyCat = originalMyCat.MyClone();
            var cloneMyCat = (MyCat)originalMyCat.Clone();
            
            originalMyCat.GrowUp();
            originalMyCat.Speak();
            originalMyCat.LookPigeons();
            originalMyCat.EatRabbitTail();
            
            Console.WriteLine(originalMyCat.ToString());
            Console.WriteLine(myCloneMyCat.ToString());
            Console.WriteLine(cloneMyCat.ToString());
        }
        
        static void CloneHumanDemo()
        {
            var originalHuman = new Human(18, Sex.Male, "Ivan", "Ivanov");
            var myCloneHuman = originalHuman.MyClone();
            var cloneHuman = (Human)originalHuman.Clone();
            
            originalHuman.Speak();
            
            Console.WriteLine(originalHuman.ToString());
            Console.WriteLine(myCloneHuman.ToString());
            Console.WriteLine(cloneHuman.ToString());
        }
        
        static void CloneProgrammerDemo()
        {
            var originalProgrammer = new Programmer(18, Sex.Male, "Ivan", "Ivanov", "C#");
            var myCloneProgrammer = originalProgrammer.MyClone();
            var cloneProgrammer = (Programmer)originalProgrammer.Clone();
            
            originalProgrammer.Speak();
            originalProgrammer.JoinMeeting();
            originalProgrammer.GrowUp();
            
            Console.WriteLine(originalProgrammer.ToString());
            Console.WriteLine(myCloneProgrammer.ToString());
            Console.WriteLine(cloneProgrammer.ToString());
        }
        
        static void CloneStudentDemo()
        {
            var originalStudent = new Student(18, Sex.Male, "Ivan", "Ivanov");
            var myCloneStudent = originalStudent.MyClone();
            var cloneStudent = (Student)originalStudent.Clone();
            
            originalStudent.Speak();
            originalStudent.SkipLecture();
            originalStudent.MakeHomeWork();
            
            Console.WriteLine(originalStudent.ToString());
            Console.WriteLine(myCloneStudent.ToString());
            Console.WriteLine(cloneStudent.ToString());
        }
    }
}