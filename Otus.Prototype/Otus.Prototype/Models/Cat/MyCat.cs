using System;
using Otus.Prototype.Interfaces;

namespace Otus.Prototype.Models
{
    /// <summary>
    /// Класс мой кот
    /// </summary>
    public class MyCat : Cat, IMyCloneable<MyCat>, ICloneable
    {
        public int RabbitTails { get; set; }
        
        /// <summary>
        /// Класс мой кот
        /// </summary>
        /// <param name="age">Возраст</param>
        /// <param name="sex">Пол</param>
        /// <param name="nickname">Кличка</param>
        public MyCat(int age, Sex sex, string nickname) : base(age, sex, nickname)
        {
        }

        private MyCat(MyCat instance) : base(instance.Age, instance.Sex, instance.Nickname)
        {
            RabbitTails = instance.RabbitTails;
        }

        public override void Speak()
        {
            Console.WriteLine("Meow!Meow?MEEEEOOOW!!!");
        }

        public void LookPigeons()
        {
            Console.WriteLine("I want to fly with them..Meow?");
        }

        public void EatRabbitTail()
        {
            Console.WriteLine("Hrum..hrum..mrrr..meow?..hrmmm..");
            --RabbitTails;
        }

        public override MyCat MyClone()
        {
            return new MyCat(this);
        }
        
        public override object Clone()
        {
            return new MyCat(this);
        }
        
        public override string ToString()
        {
            return $"MyCat.{Environment.NewLine}" +
                   $"Age: {Age}.{Environment.NewLine}" +
                   $"Sex: {Sex}.{Environment.NewLine}" +
                   $"Nickname: {Nickname}.{Environment.NewLine}" +
                   $"RabbitTails: {RabbitTails}.{Environment.NewLine}";
        }
    }
}