using System;
using Otus.Prototype.Interfaces;

namespace Otus.Prototype.Models
{
    /// <summary>
    /// Класс кот
    /// </summary>
    public class Cat : Mammals, IMyCloneable<Cat>, ICloneable
    {
        public string Nickname { get; }
        
        /// <summary>
        /// Класс кот
        /// </summary>
        /// <param name="age">Возраст</param>
        /// <param name="sex">Пол</param>
        /// <param name="nickname">Кличка</param>
        public Cat(int age, Sex sex, string nickname) : base(age, sex)
        {
            Nickname = nickname;
        }

        private Cat(Cat instance) : base(instance.Age, instance.Sex)
        {
            Nickname = instance.Nickname;
        }

        public override void Speak()
        {
            Console.WriteLine("Meow?");
        }

        public override void GrowUp()
        {
            Age += 4;
        }

        public virtual Cat MyClone()
        {
            return new Cat(this);
        }

        public virtual object Clone()
        {
            return new Cat(this);
        }

        public override string ToString()
        {
            return $"Cat.{Environment.NewLine}" +
                   $"Age: {Age}.{Environment.NewLine}" +
                   $"Sex: {Sex}.{Environment.NewLine}" +
                   $"Nickname: {Nickname}.{Environment.NewLine}";
        }
    }
}