using System;
using System.IO;
using Otus.Prototype.Interfaces;

namespace Otus.Prototype.Models
{
    /// <summary>
    /// Класс млекопитающие
    /// </summary>
    public abstract class Mammals
    {
        protected int Age;
        protected readonly Sex Sex;
        
        /// <summary>
        /// Класс млекопитающие
        /// </summary>
        /// <param name="age">Возраст</param>
        /// <param name="sex">Пол</param>
        protected Mammals(int age, Sex sex)
        {
            Age = age;
            Sex = sex;
        }

        public abstract void Speak();

        public virtual void GrowUp()
        {
            Age += 1;
        }
    }
}