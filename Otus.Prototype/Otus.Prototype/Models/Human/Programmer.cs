using System;
using Otus.Prototype.Interfaces;

namespace Otus.Prototype.Models
{
    /// <summary>
    /// Класс программист
    /// </summary>
    public class Programmer : Human, IMyCloneable<Programmer>, ICloneable
    {
        public string ProgrammingLanguage { get; set; }
        public bool IsBurnOut { get; set; }
        
        /// <summary>
        /// Класс программист
        /// </summary>
        /// <param name="age">Возраст</param>
        /// <param name="sex">Пол</param>
        /// <param name="firstname">Имя</param>
        /// <param name="surname">Фамилия</param>
        /// <param name="programmingLanguage">Язык программирования</param>
        public Programmer(int age, Sex sex, string firstname, string surname, string programmingLanguage) : base(age, sex, firstname, surname)
        {
            ProgrammingLanguage = programmingLanguage;
        }

        private Programmer(Programmer instance) : base(instance.Age, instance.Sex, instance.Firstname, instance.Surname)
        {
            ProgrammingLanguage = instance.ProgrammingLanguage;
            IsBurnOut = instance.IsBurnOut;
        }

        public override void Speak()
        {
            Console.WriteLine("Leave me alone, I'm writing code.");
        }

        public void JoinMeeting()
        {
            IsBurnOut = true;
        }

        public override Programmer MyClone()
        {
            return new Programmer(this);
        }

        public override object Clone()
        {
            return new Programmer(this);
        }
        
        public override string ToString()
        {
            return $"Programmer.{Environment.NewLine}" +
                   $"Age: {Age}.{Environment.NewLine}" +
                   $"Sex: {Sex}.{Environment.NewLine}" +
                   $"Firstname: {Firstname}.{Environment.NewLine}" +
                   $"Surname: {Surname}.{Environment.NewLine}" +
                   $"ProgrammingLanguage: {ProgrammingLanguage}.{Environment.NewLine}" +
                   $"IsBurnOut: {IsBurnOut}.{Environment.NewLine}";
        }
    }
}