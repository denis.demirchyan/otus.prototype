using System;
using Otus.Prototype.Interfaces;

namespace Otus.Prototype.Models
{
    /// <summary>
    /// Класс студент
    /// </summary>
    public class Student : Human, IMyCloneable<Student>, ICloneable
    {
        private int _studyDebts;
        
        /// <summary>
        /// Класс студент
        /// </summary>
        /// <param name="age">Возраст</param>
        /// <param name="sex">Пол</param>
        /// <param name="firstname">Имя</param>
        /// <param name="surname">Фамилия</param>
        public Student(int age, Sex sex, string firstname, string surname) : base(age, sex, firstname, surname)
        {
        }

        private Student(Student instance) : base(instance.Age, instance.Sex, instance.Firstname, instance.Surname)
        {
            _studyDebts = instance._studyDebts;
        }

        public void SkipLecture()
        {
            _studyDebts += 2;
        }

        public void MakeHomeWork()
        {
            if (_studyDebts > 0)
            {
                --_studyDebts;
            }
        }

        public override void Speak()
        {
            Console.WriteLine("I'm so tired..I want to sleep..");
        }

        public override Student MyClone()
        {
            return new Student(this);
        }
        
        public override object Clone()
        {
            return new Student(this);
        }

        public override string ToString()
        {
            return $"Student.{Environment.NewLine}" +
                   $"Age: {Age}.{Environment.NewLine}" +
                   $"Sex: {Sex}.{Environment.NewLine}" +
                   $"Firstname: {Firstname}.{Environment.NewLine}" +
                   $"Surname: {Surname}.{Environment.NewLine}" +
                   $"StudyDebts: {_studyDebts}.{Environment.NewLine}";
        }
    }
}