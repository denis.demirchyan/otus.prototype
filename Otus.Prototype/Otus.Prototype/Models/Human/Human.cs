using System;
using Otus.Prototype.Interfaces;

namespace Otus.Prototype.Models
{
    /// <summary>
    /// Класс человек
    /// </summary>
    public class Human : Mammals, IMyCloneable<Human>, ICloneable
    {
        public string Firstname { get; }
        public string Surname { get; }
        
        /// <summary>
        /// Класс человек
        /// </summary>
        /// <param name="age">Возраст</param>
        /// <param name="sex">Пол</param>
        /// <param name="firstname">Имя</param>
        /// <param name="surname">Фамилия</param>
        public Human(int age, Sex sex, string firstname, string surname) : base(age, sex)
        {
            Firstname = firstname;
            Surname = surname;
        }

        private Human(Human instance) : base(instance.Age, instance.Sex)
        {
            Firstname = instance.Firstname;
            Surname = instance.Surname;
        }

        public override void Speak()
        {
            Console.WriteLine("I'm human.");
        }

        public virtual Human MyClone()
        {
            return new Human(this);
        }

        public virtual object Clone()
        {
            return new Human(this);
        }
        
        public override string ToString()
        {
            return $"Human.{Environment.NewLine}" +
                   $"Age: {Age}.{Environment.NewLine}" +
                   $"Sex: {Sex}.{Environment.NewLine}" +
                   $"Firstname: {Firstname}.{Environment.NewLine}" +
                   $"Surname: {Surname}.{Environment.NewLine}";
        }
    }
}